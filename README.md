# Reindeer-Discord-Bot
A very simple discord bot to share the christmas spirit by having all the reindeer join a VC in your guild. :)

# Status:
All offline until next year :(
 
# Links:
Dasher - https://discord.com/api/oauth2/authorize?client_id=789419994910818334&permissions=8&scope=bot

Dancer - https://discord.com/api/oauth2/authorize?client_id=789418944989626408&permissions=8&scope=bot

Prancer  - https://discord.com/api/oauth2/authorize?client_id=789454241977860128&permissions=8&scope=bot

Vixen - https://discord.com/api/oauth2/authorize?client_id=789455003508539393&permissions=8&scope=bot

Comet  - https://discord.com/api/oauth2/authorize?client_id=789417819929575465&permissions=8&scope=bot

Cupid  - https://discord.com/api/oauth2/authorize?client_id=789418381920829460&permissions=8&scope=bot

Donner - https://discord.com/api/oauth2/authorize?client_id=789456116881817650&permissions=8&scope=bot

Blitzen - https://discord.com/api/oauth2/authorize?client_id=789415712280018954&permissions=8&scope=bot

Rudolph  - https://discord.com/api/oauth2/authorize?client_id=789404709366661170&permissions=8&scope=bot

## Want your own reindeer pen?
Just invite the bots you want, and make a channel titled `Reindeer Pen`, they will automatically join on the next message being sent. Please make sure they have perms to `CONNECT` to the voice channel.
Please note that these bots will only be hosted publicly during the month of December. 
