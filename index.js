const Discord = require("discord.js");
const client = new Discord.Client();

client.on("ready", () => {
  console.log(`${client.user.tag} is connected to the Discord API.`);
  client.user.setPresence({
    activity: { name: `with reindeer things.` },
    status: "dnd",
  });
  console.log("Presence set to idle.");

  setInterval(() => {
    client.user.setPresence({
      activity: { name: `with reindeer things.` },
      status: "dnd",
    });
    console.log("Presence Updated");
  }, 300000);
});

client.on("message", (message) => {
  const ch = message.guild.channels.cache.find(
    (channel) => channel.name === "Reindeer Pen"
  );
  if (!ch) return;
  ch.join()
    .then((connection) => {})
    .catch((e) => {
      console.error(e);
    });
});

client.login("");
